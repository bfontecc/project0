<?php
/*
 * header.php
 * by Bret Fontecchio
 *
 * Constructs a header dynamically.
 */
 ?>
<!DOCTYPE html>
<?php $title_prepend = "Three Aces Pizza -- "; ?>
<html>
    <head>
        <title> 
            <?php echo htmlspecialchars($title_prepend.$title) ?>
        </title>

    <!--Twitter Bootstrap CSS Library-->
    <!--license: https://github.com/twitter/bootstrap/wiki/License-->
    <link rel="stylesheet" href="/bootstrap/docs/assets/css/bootstrap.css">
    
    <style type="text/css">
    body { 
    	padding-top: 55px;
    	background-image: url('/images/bg.png');  
		background-repeat:repeat; 
	}
	</style>
	<!-- 
	background image source:
		http://mjtweaver.com/dev/wp-content/themes/stilo/images/header-bg.png
	license:
		not sure, but it was on built with bootstrap
		also, this seems to be fair use (academic)
	-->
	<?php include_once("../views/templates/nav_bar.php"); ?>
	<?php include_once("../views/templates/title_well.php"); ?>
    </head>
    <body>

