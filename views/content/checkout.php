<div class="hero-unit">
        <h1>Thank You</h1>
        <p>Your order has been processed (not really, though!)</p>
        <p>
        	You will be charged: 
        	$<?php echo number_format($cart_data['total'], 2) ?>
        	(okay, maybe not.)
        </p>
        <p><a class="btn btn-primary btn-large" href="index.php?q=home">Home »</a></p>
</div>
<?php session_destroy(); ?>