<?php
/**
 * display menu outer
 * creates bootstrap pills indicating category
 * sets ?q=menu&category=... in GET
 */
echo "<ul class=\"nav nav-tabs\">"; 

foreach($menu_data['categories'] as $category) {
	echo "<li";
	if (isset($_GET['category'])) {
		if ($category == $_GET['category']) {
			print(" class=\"active\">");
		} else {
			print(">");
		}
	} else {
		print(">");
	}
	echo "<a href=\"index.php?q=menu&amp;category=" . $category . "\">";
	echo $category;
	echo "</a>";
	echo "</li>";
}
echo "</ul>";
?>