<?php
/**
 * menu_add_form.php
 *
 * posts new item to add to cart.
 * item is posted to controller as:
 * $_POST	[
 			'item_add' 			=> true
 			'item_add_quantity' => string
 			'item_add_size		=> string
 			'item_add_category'	=> string
 			'item_add_type'		=> string
 			'item_add_price'	=> string
 			]
 */
 
$cat = $_GET['category'];
$t = $_GET['type'];
echo "<h3>" . $cat . " - " . $t . "</h3>";

//create inline forms roughly similar to those in cart_edit.php
foreach($menu_data['item_data'] as $item):
	$size = "error: can't process size";
	$price = "error: can't determine price";
	$quantity = "1";
	if (isset($item['size'])) {
		$size = $item['size'];
	}
	if (isset($item['price'])) {
		$price = $item['price'];
	}
	?>
	<form class="well form-inline" method="post" action="index.php?q=edited_message">
		<input type="hidden" name="item_add_category" value=<?php echo "\"". $cat . "\""; ?>>
		<input type="hidden" name="item_add_type" value=<?php echo "\"". $type . "\""; ?>>
		<input type="hidden" name="item_add" value="true">
		<input type="hidden" name="item_add_size" value=<?php echo "\"". $size . "\""; ?> >
		<input type="hidden" name="item_add_price" value=<?php echo "\"". $price . "\""; ?> >
		<label>size: <?php echo $size; ?></label>
		<label> | price: <?php echo $price; ?></label>
		
		<?php 
		/* add radio buttons if there are options in the menu here */
		if (isset($menu_data['option_data']) && $menu_data['option_data'] != false) {
			$num_opt = 0;
			foreach($menu_data['option_data'] as $option) {
				echo "<input type='radio' name='item_option' value='" . $option . "'";
				if ($num_opt == 0) {
					echo " checked ";
				}
				echo ">";
				echo htmlspecialchars($option);
				$num_opt++;
			}
		}
		?>
		<button type="submit" class="btn pull-right">Add to Cart</button>
		<input type="text" name="item_add_quantity"  class="input-mini pull-right" value="1" />
		<label class="pull-right">quantity:&nbsp;&nbsp;&nbsp;</label>
		
	</form>
<?php endforeach ?>