<h3 align="center">Your shopping cart has been updated!</h3>

<br/><br/>

<a class="btn btn-large pull-left" href="index.php?q=cart_edit">
	Edit Cart.
</a>
<a class="btn btn-large pull-left" href="index.php?q=menu">
	Keep Shopping!
</a>

<a class="btn btn-primary btn-large pull-right" href="index.php?q=checkout">
	Check Out»
</a>

<br/><br/>