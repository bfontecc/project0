<?php
/**
 * cart_edit.php
 *
 * dynamically creates and renders a form for the user to edit cart data.
 *
 */ 
 
//$cart_data comes from controller
if (!isset($cart_data['is_empty']) || $cart_data['is_empty'] == true) {
	echo "<h3>Your Cart is currently empty.</h2><br/>";
	echo "<a class=\"btn btn-primary btn-large\" href=\"index.php?q=menu\">Menu »</a>";
}
$index = 0;
foreach($cart_data['items'] as $cart_item): ?>
<form class="well form-inline" method="post" action="index.php?q=edited_message">
	<input type="hidden" name="cart_form_index" value=<?php echo "\"" . $index . "\""; ?> />
	<label>Item:&nbsp; <?php echo $index+1 ?> &nbsp;&nbsp;&nbsp; </label>
	<label class="checkbox"/>
		<input type="checkbox" name="item_delete" value=<?php echo "\"" . $index . "\""; ?>/> delete
	</label>
	
	<label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
	
	<?php echo "<label>" . $cart_item['category'] . " -- " . $cart_item['type'] . "</label>"; ?>
	
	<button type="submit" class="btn pull-right">Change Item</button>
	<input type="text" name="new_quantity"  class="input-mini pull-right" value=<?php echo "\"" . $cart_item['quantity'] . "\""; ?> />
	<label class="pull-right">quantity:&nbsp;&nbsp;&nbsp;</label>
	
	<br>
	<label>
		price: $<?php echo number_format($cart_item['price'], 2); ?>
		&nbsp;x&nbsp;<?php echo $cart_item['quantity']; ?>
	</label>
	
</form>

<?php
$index++;
endforeach 
?>

<div class="well">
	Total Price: $
	<?php echo number_format($cart_data['total'], 2); ?>
	<a class="btn btn-primary btn-large pull-right" href="index.php?q=checkout">
		Check Out!
	</a>
</div>
	