<?php
session_start();

//sync cart to session
include_once("../models/cart_model.php");
//load menu model
include_once("../models/menu_model.php");

//create_test_cart();	//debugging test cart

//but not this
$cart_data = update_cart();

//determine what get requests are asking for content (home is default)
//this will set the variables $title and $content used in content_loader
extract(parse_get());

//header, including title, nav-bar, title well
include_once("../views/templates/header.php");
//content
include_once("../views/content_loader.php");
include_once("../views/templates/footer.php");

function parse_get() {
	$menu_data = array();
	if (!isset($_GET['q'])) {
		$_GET['q'] = 'home';
	}
	switch ($_GET['q']) {
		default:
			$title = "Home";
			$content = "../views/content/home.php";
			break;
		case 'home':
			$title = "Home";
			$content = "../views/content/home.php";
			break;
		case 'cart_edit':
			$title = "Edit Cart";
			$content = "../views/content/cart_edit.php";
			break;
		case 'menu':
			$title = "Menu";
			$content = "../views/content/menu/menu.php";
			$menu_data['categories'] = get_menu_categories();
			if (isset($_GET['category'])) {
				$menu_data['types'] = get_menu_types($_GET['category']);
				if (isset($_GET['type'])) {
					$menu_data['item_data'] = get_item_data($_GET['category'], $_GET['type']);
					$menu_data['option_data'] = get_option_data($_GET['category'], $_GET['type']);
				}
			}
			break;
		case 'contact':
			$title = "Contact us";
			$content = "../views/content/contact.php";
			break;
		case 'edited_message':
			$title = "OK";
			$content = "../views/content/edited_message.php";
			break;
		case 'checkout':
			$title = "Checkout";
			$content = "../views/content/checkout.php";
			break;
	}
	return array(	'title' 	=> $title,
					'content' 	=> $content,
					'menu_data' => $menu_data
				);
}

function update_cart() {
	if (isset($_POST['item_add']) && $_POST['item_add'] == true) {
		$item_package = array(
							'category' => htmlspecialchars($_POST['item_add_category']),
							'type' => htmlspecialchars($_POST['item_add_type']),
							'size' => htmlspecialchars($_POST['item_add_size']),
							'quantity' => intval(htmlspecialchars($_POST['item_add_quantity'])),
							'price' => htmlspecialchars($_POST['item_add_price'])
							);
		if (isset($_POST['item_option']) && $_POST['item_option'] != false) {
			$item_package['category'] = $_POST['item_option'];
		}
		if (add_item($item_package)) {
			//do nothing.
			//something could go here in the future.
		} else {
			echo "<script>alert('Error: encountered an error adding item.');</script>";
		}
	}
	if (isset($_POST['item_delete'])) {
		//passes item index number
		remove_item($_POST['item_delete']);
	}
	if (isset($_POST['new_quantity'])) {
		$qc_index = $_POST['cart_form_index'];
		$qc_new_amount = intval($_POST['new_quantity']);
		change_quantity($qc_index, $qc_new_amount);
	}
	return get_cart_data();
}

?>