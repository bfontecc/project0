<?php
/**
 * menu_model.php
 *
 * Requires menu.xml
 * Takes queries from controller, and parses menu to return pertinent data
 */
 
$xml = simplexml_load_file("../models/menu.xml");

/**
 * function build_menu_outer()
 *
 * returns an array containing category names
 */
function get_menu_categories() {
	$cat_array = array();
	global $xml;
	$cats = $xml->xpath("/menu/category/name");
	foreach($cats as $cat) {
		array_push($cat_array, $cat);
	}
	return $cat_array;
}

function get_menu_types($category) {
	$type_array = array();
	global $xml;
	$xpath_string = "/menu/category[name='" . $category . "']";
	$cat = $xml->xpath($xpath_string);
	if (!isset($cat[0])) {
		return false;
	}
	$in_cat = $cat[0]->xpath("type");
	foreach($in_cat as $tmp) {
		array_push($type_array, $tmp->name);
	}
	return $type_array;
}

/**
 * function get_option_data($category, $type)
 *
 * returns a numeric array of strings
 */
function get_option_data($category, $type) {
	$option_array = array();
	global $xml;
	$xpath_string = "/menu/category[name='" . $category . "']";
	$cat = $xml->xpath($xpath_string);
	if (!isset($cat[0])) {
		return false;
	}
	$level_one = $cat[0]->xpath('options');
	if (!isset($level_one[0])) {
		return false;
	}
	$level_two = $level_one[0]->xpath('option');
	foreach ($level_two as $option_string) {
		$option_string = (string)$option_string;
		array_push($option_array, $option_string);
	}
	return $option_array;
}

/*
 * function match_cat_and_type()
 *
 * should return an array, item 0 of which is a SimpleXMLElement object
 * holding name, option, and price data for that category/type
 */
function match_cat_and_type($category, $type) {
	$match_array = array();
	global $xml;
	$xpath_string = "/menu/category[name='" 
					. $category 
					. "']/type[name='"
					.$type
					."']";
	$match = $xml->xpath($xpath_string);
	return $match;
}

/**
 * function get_item_data($category, $type)
 *
 * returns a numeric array of associative arrays
 * [0] => Array
 *	(
 *		'size' 	=> string
 *		'price' => string
 *	)
 */
function get_item_data($category, $type) {
	$item_array = array();
	$type_data = match_cat_and_type($category, $type);
	if (!isset($type_data[0])) {
		return false;
	}
	$sizes_array = $type_data[0]->xpath("size");
	foreach($sizes_array as $size_object) {
		if (!isset($size_object->label) || !isset($size_object->price)) {
			return false;
		}
		$this_size_data = array(
								'size' => (string)$size_object->label, 
								'price' => (string)$size_object->price
								);
		array_push($item_array, $this_size_data);
	}
	
	return $item_array;
}

