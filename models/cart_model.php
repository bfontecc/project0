<?php
/**
 * cart_model.php
 *
 * manages session data regarding shopping cart.
 *
 * cart_model offers its functions to the controller.
 */
 
 
 /**
  * function create_test_cart()
  *
  * takes no parameters, creates cart with two items, one of which has quantity 2
  * The total cart count should be 3. will overwrite previous contents.
  */
 function create_test_cart() {
    if (isset($_SESSION['cart'])) {
        unset($_SESSION['cart']);
    }
    if (!isset($_SESSION['cart'])) {
        $_SESSION['cart'] = array();
        array_push($_SESSION['cart'], array(
                                            "category"  => "foo_pizza",
                                            "type"      => "cheese",
                                            "size"      => "small",
                                            "quantity"  => "1",
                                            "price"		=> "8.50"
                                            ));
        array_push($_SESSION['cart'], array(
                                            "category"  => "bar",
                                            "type"      => "baz",
                                            "size"      => "massive",
                                            "quantity"  => "2",
                                            "price"		=> "10.00"
                                            ));
    }
}

/**
 * funtion print_cart()
 *
 * for debugging. 
 * takes no params, prints cart from $_SESSION.
 */
function print_cart() {
	print_r($_SESSION['cart']);
}

/**
 * count_items()
 *
 * Counts the number of items in the cart (in session). Returns an int.
 */
function count_items() {
    $count = 0;
    if (isset($_SESSION['cart'])) {
        foreach ($_SESSION['cart'] as $item) {
            if (isset($item['quantity'])) {
                $count += intval($item['quantity']);
            } else {
                $count++;
            }
        }
    }
    return $count;
}

/**
 * function calc_price_total()
 *
 * adds up all the prices in cart array, as floats
 * Rounds to nearest cent. Returns false on failure
 */
function calc_price_total() {
	$total = floatval(0.00);
    if (isset($_SESSION['cart'])) {
        foreach ($_SESSION['cart'] as $item) {
            if (isset($item['price'])) {
                $total += floatval($item['price']) * floatval($item['quantity']);
            } else {
                return false;
            }
        }
    }
    return $total;
}

/**
 * funtion add_item
 * accepts as param an associative array, which should have values for:
 * (
 *      'category' =>
 *      'type' =>
 *      'size' =>
 *      'quantity' =>
 * )
 * Returns false if array malformed, true if item add statement executed.
 */
function add_item($new_item) {
	if ( isset($new_item['category'])
		&& isset($new_item['type'])
		&& isset($new_item['size'])
		&& isset($new_item['quantity'])
		&& isset($new_item['price'])
	 	) {
	 		array_push($_SESSION['cart'], $new_item);
	 		return true;
	 } else { return false; }
}

/**
 * function remove_item()
 * Accepts as param an index into the cart's numerical array, associated with
 * key 'cart'.
 *
 * important: This function re-keys the array, changing indices for other items.
 */
function remove_item($cart_index) {
	if (isset($_SESSION['cart'][$cart_index])) {
		unset($_SESSION['cart'][$cart_index]);
		//re-key
		$_SESSION['cart'] = array_values($_SESSION['cart']);
	}
}

/**
 * function change_quantity()
 * Accepts as params an item index and a new quantity
 */
function change_quantity($index, $quant) {
	if (isset($_SESSION['cart'][$index])) {
		if (intval($quant <= 0)) {
			remove_item($index);
		} else {
			$_SESSION['cart'][$index]['quantity'] = $quant;
		}
	}
}

/**
 * function get_cart_data()
 * returns an array, holding 'count' and 'is_empty' keys,
 * also holds $_SESSION['cart'] in $cart_data['items']
 */ 
function get_cart_data() {
	if (!isset($_SESSION['cart'])) {
		$_SESSION['cart'] = array();
	}
	$count = count_items();
	$total = calc_price_total();
	$is_empty = true;
	$is_empty = ($count > 0) ? false : true;
	$cart_data = array (
				'count'		 => $count,
				'total'		 => $total,
				'is_empty'	 => $is_empty,
				'items'		 => $_SESSION['cart']
				);
	return $cart_data;
}
				
?>