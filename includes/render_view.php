<?php
/**
 * render_view.php
 *
 * Accepts an array of variables, creates local variables storing params,
 * uses require_once() to load views.
 *
 * Heavily based on Professor Malan's /mvc/includes/helpers.php
 *
 */

function render($path_from_views, $params=array()) {
    $path_to_views = "../views";
    $path = $path_to_views . $path_from_views;
    if (file_exists($path)) {
        // cover the corner case where something is passed,
        // but it's not an array
        if (isset($params) && is_array($params)) {
            extract($params);
        }
        require($path);
    }
}
?>
